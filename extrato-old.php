<?php

// Metodo GET "extrato.php?id="
$cpf = isset($_GET['id']) ? $_GET['id'] : null;


if (!$cpf) {
    die("ID Invalido");
}

//$cpf = "123456789-01";

// função de decremento de data
$dt = new DateTime();
$periodoFim = $dt->format( "Y-m-d" );
$dt->sub( new DateInterval( "P15D" ) ); // subtrai 15 dias
$periodoInicio = $dt->format( "Y-m-d" );

// Curl de requisição do extrato
$curl = curl_init();

 curl_setopt_array($curl, array(
    CURLOPT_URL => "https://fy4gfo7nbl.execute-api.sa-east-1.amazonaws.com/ApiSouPi/carteira/int-car-cco-get-extrato",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{\r\n \"cod_empresa\": 1,\r\n \"agencia\": 1,\r\n \"id_cliente\": $cpf ,\r\n \"dt_inicio_consulta\": \"$periodoInicio\" ,\r\n \"dt_fim_consulta\": \"$periodoFim\" \r\n}",
    CURLOPT_HTTPHEADER => array(
        "x-api-key: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        "Content-Type: application/json",
        "Content-Type: application/json"
    ) , ));
$response = curl_exec($curl);
//var_dump($response);
// die();

header("Cache-Control: no-cache, no-store, must-revalidate");
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
echo $response;


//$objResponse = json_decode($response);
//var_dump($objResponse);


//foreach($objResponse->data->Item as $item) {
//    var_dump($item);
//    var_dump($item->pXvaTipo);
//}
